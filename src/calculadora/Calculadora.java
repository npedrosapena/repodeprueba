package calculadora;

import java.util.Scanner;

/**
 * Clase para calcular varias operaciones de 2 numeros
 * 
 * @author Nelson Pedrosa Pena
 * @version Version 1.0
 * 
 */

public class Calculadora {
     static float NUMERO1;
     static float NUMERO2;
     
        static Scanner dato = new Scanner(System.in,"UTF-8");
/** 
* main del programa
*/
    public static void main(String[] args) 
    {
        //peticion datos

            System.out.print("Intoduzca valor 1: ");
                NUMERO1=dato.nextFloat();
           
            System.out.print("Introduzca valor 2: ");
                NUMERO2=dato.nextFloat();
              
             //llamada al menu
                menu();
              
            //realizar la operación
                elSwitch();         
    }
    
    // ZONA DE FUNCIONES
    
    public static void elSwitch()
    {
         switch (dato.nextInt())
                {
                    case 1: imprimirPantalla(suma(NUMERO1,NUMERO2));
                        break;
                    case 2:imprimirPantalla(resta(NUMERO1,NUMERO2));
                        break;
                    case 3: imprimirPantalla(multi(NUMERO1,NUMERO2));
                        break;
                    case 4:imprimirPantalla(division(NUMERO1,NUMERO2));
                        break;
                    default: System.out.println("fin");
                }
    }
    
  /**
   * Suma 2 números
   * @param num1 valor del primer número
   * @param num2 valor del segundo número
   * @return resultadoOperacion suma de dos numeros 
   */
    public static float suma(float num1, float num2)
        {
            //declaración variable
            float resultadoOperacion;
            
                resultadoOperacion=num1+num2;
                
                //devolución
            return resultadoOperacion;
        }
   
    /**
   * Resta 2 números
   * @param num1 valor del primer número
   * @param num2 valor del segundo número
   * @return resultadoOperacion resta de dos numeros 
   */
    public static float resta(float num1, float num2)
        {
            //declaración variable
            float resultadoOperacion;
            
                resultadoOperacion=num1-num2;
                
                //devolución
            return resultadoOperacion;
        }
    
     /**
   * multiplicación 2 números
   * @param num1 valor del primer número
   * @param num2 valor del segundo número
   * @return resultadoOperacion multiplicación de dos numeros 
   */
    public static float multi(float num1, float num2)
        {
            //declaración variable
            float resultadoOperacion;
            
                resultadoOperacion=num1*num2;
                
                //devolución
            return resultadoOperacion;
        }
    
     /**
   * división 2 números
   * @param num1 valor del primer número
   * @param num2 valor del segundo número
   * @return resultadoOperacion división de dos numeros 
   */
    public static float division(float num1, float num2)
        {
            //declaración variable
            float resultadoOperacion;
            
                resultadoOperacion=num1/num2;
                
                //devolución
            return resultadoOperacion;
        }
    
      /**
       * Saca por pantalla el resultado de la operación
       * @param resultado 
       */
    
    public static void imprimirPantalla(float resultado)
        {
            System.out.println("el resultado de la operación es: "+resultado);
        }
    
    /**
     * menu
     */
    public static void menu()
    {
            System.out.println("1.- Suma");
            System.out.println("2.- Resta");
            System.out.println("3.- Multiplicación");
            System.out.println("4.- División");
            System.out.println("");
            System.out.print("Introduzca la operación: ");
    }
}
